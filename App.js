import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  PermissionsAndroid,
  Alert,
  Picker,
  ToastAndroid,
  TextInput,
} from 'react-native';
import TwilioVoice from 'react-native-twilio-programmable-voice';
import {PhoneNumberUtil, PhoneNumberFormat} from 'google-libphonenumber';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import qs from 'qs';
import {get} from 'lodash';

const PNF = PhoneNumberFormat;
const phoneUtil = PhoneNumberUtil.getInstance();

export default class App extends Component {
  //path
  // http://4f982061.ngrok.io/accesstoken

  state = {
    testPath: '9fdbe5a4',
    twilioInited: false,
    identity: 'nubrabra01',
    phoneNumber: '',
    connected: false,
  };

  _storeData = async () => {
    try {
      const twilioInited = this.state.twilioInited;
      const identity = this.state.identity;
      const connected = this.state.connected;
      const dataTwilio = JSON.stringify({twilioInited, identity, connected});
      await AsyncStorage.setItem('twilio', dataTwilio);
    } catch (error) {}
  };

  _retrieveData = async () => {
    try {
      const twilio = await AsyncStorage.getItem('twilio');
      console.log('AsyncStorage getItem twilio ::', twilio);
      const dataTwilio = JSON.parse(twilio);
      if (twilio !== null) {
        const twilioInited = get(dataTwilio, 'twilioInited', '');
        const identity = get(dataTwilio, 'identity', '');
        const connected = get(dataTwilio, 'connected', '');
        console.log(
          'twilioInited::',
          twilioInited,
          ':: identity ::',
          identity,
          ':: connected ::',
          connected,
        );
        this.setState({twilioInited, identity, connected});
        return true;
      }
      return false;
    } catch (error) {
      return false;
    }
  };

  // componentWillMount() {
  //   const hasTwilio = this._retrieveData();
  //   if (!hasTwilio) {
  //     this.initTwilio();
  //   }
  //   TwilioVoice.addEventListener('deviceReady', () => this.setState({ twilioInited: true }, () => this._storeData()));
  //   TwilioVoice.addEventListener('connectionDidConnect', () => this.setState({ connected: true }, () => this._storeData()));
  //   TwilioVoice.addEventListener('connectionDidDisconnect', () => this.setState({ connected: false }, () => this._storeData()));
  //   // should be called after the app is initialized
  //   // to catch incoming call when the app was in the background
  //   TwilioVoice.getActiveCall()
  //     .then(incomingCall => {
  //       console.log('incomingCall::', incomingCall);
  //       // ToastAndroid.show(`incomingCall::${incomingCall}`, ToastAndroid.LONG);
  //       if (incomingCall) {
  //         // _deviceDidReceiveIncoming(incomingCall)
  //       }
  //     }
  //     )
  //   if (Platform.OS === 'ios') TwilioVoice.configureCallKit({ appName: 'ReactNativeTwilioExampleApp' }); //required for ios
  // }

  getMicrophonePermission = () => {
    const audioPermission = PermissionsAndroid.PERMISSIONS.RECORD_AUDIO;
    return PermissionsAndroid.check(audioPermission).then(async result => {
      if (!result) {
        const granted = await PermissionsAndroid.request(audioPermission, {
          title: 'Microphone Permission',
          message:
            'App needs access to you microphone ' +
            'so you can talk with other users.',
        });
      }
    });
  };

  getAuthToken = () => {
    console.log(
      'selectPath ::',
      `https://${this.state.testPath}.ngrok.io/accesstoken`,
    );
    let data = qs.stringify({identity: this.state.identity});
    return axios
      .post(`https://${this.state.testPath}.ngrok.io/accesstoken`, data)
      .then(response => response.data)
      .catch(error => console.log('error getAuthToken::', error));
  };

  initTwilio = async () => {
    const token = await this.getAuthToken();
    console.log('initTwilio token ::', token);
    if (token) {
      if (Platform.OS === 'android') {
        await this.getMicrophonePermission();
      }

      await TwilioVoice.initWithToken(token);

      TwilioVoice.addEventListener('deviceReady', res => {
        console.log('deviceReady res::::', res);
        this.setState({twilioInited: true});
      });

      TwilioVoice.addEventListener('deviceNotReady', res => {
        console.log('deviceNotReady::::', res);
      });

      TwilioVoice.addEventListener('connectionDidConnect', res => {
        console.log('connectionDidConnect:::', res);
      });

      TwilioVoice.addEventListener('connectionDidDisconnect', res => {
        console.log('connectionDidDisconnect:::', res);
      });

      if (Platform.OS === 'ios') {
        TwilioVoice.configureCallKit({appName: 'TestProject'});
      }
    }
  };

  // 0893016757
  makeCall = () => {
    console.log('makeCall ::', this.state.phoneNumber);
    // const phoneNumberParse = this.onPhoneNumberParse();
    // const phoneNumberParseAndKeepRawInput = this.onPhoneNumberParseAndKeepRawInput();
    // const isValidNumber = phoneUtil.isValidNumber(phoneNumberParseAndKeepRawInput);
    // const isPossibleNumber = phoneUtil.isPossibleNumber(phoneNumberParseAndKeepRawInput);
    // if (isValidNumber && isPossibleNumber) {
    //   TwilioVoice.connect({ To: phoneNumberParse });
    // }
    TwilioVoice.connect({To: this.state.phoneNumber});
  };

  onPhoneNumberChange = data => {
    const phoneNumber = this.state.phoneNumber + data;
    if (phoneNumber.length <= 10) {
      this.setState({phoneNumber});
    }
  };

  onDeletePhoneNumber = () => {
    const phoneNumber = this.state.phoneNumber;
    this.setState({
      phoneNumber: phoneNumber.substring(0, phoneNumber.length - 1),
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <Picker
          selectedValue={this.state.identity}
          style={{
            height: 50,
            width: 335,
            backgroundColor: '#fff',
            marginBottom: 30,
          }}
          onValueChange={itemValue => this.setState({identity: itemValue})}
          enabled={!this.state.twilioInited}>
          <Picker.Item label="nubrabra01" value="nubrabra01" />
          <Picker.Item label="nubrabra02" value="nubrabra02" />
          <Picker.Item label="nubrabra03" value="nubrabra03" />
        </Picker>

        <TextInput
          style={{height: 50, width: 335, backgroundColor: '#fff'}}
          onChangeText={phoneNumber => this.setState({phoneNumber})}
          value={this.state.phoneNumber}
        />

        <TextInput
          style={{
            height: 50,
            width: 335,
            backgroundColor: '#fff',
            marginTop: 30,
          }}
          onChangeText={testPath => this.setState({testPath})}
          value={this.state.testPath}
        />

        <TouchableOpacity onPress={() => this.initTwilio()}>
          <Text style={styles.status}>
            Status : {this.state.twilioInited ? 'ready' : 'not ready'}
          </Text>
        </TouchableOpacity>

        <View style={styles.box}>
          <TouchableOpacity
            style={!this.state.connected ? styles.callShow : styles.callHide}
            disabled={!this.state.twilioInited}
            onPress={this.makeCall}>
            <Text>CALL</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={this.state.connected ? styles.hangupShow : styles.hangupHide}
            disabled={!this.state.connected}
            onPress={() => TwilioVoice.disconnect()}>
            <Text>HANGUP</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000',
  },
  welcome: {
    fontSize: 40,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  status: {
    marginTop: 20,
    color: '#fff',
  },
  phoneNumber: {
    margin: 5,
    fontSize: 40,
    color: '#fff',
  },
  phoneNumberLine: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 10,
  },
  phoneNumberItem: {
    margin: 10,
    height: 60,
    width: 60,
    backgroundColor: '#fff',
    borderRadius: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputPhoneNumber: {
    margin: 5,
    backgroundColor: '#fff',
    borderRadius: 5,
    borderColor: '#000',
  },
  box: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 10,
  },
  callShow: {
    display: 'flex',
    margin: 10,
    height: 80,
    width: 80,
    backgroundColor: '#64dd17',
    borderRadius: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  callHide: {
    display: 'none',
  },
  hangupShow: {
    display: 'flex',
    margin: 10,
    height: 80,
    width: 80,
    backgroundColor: '#ff1744',
    borderRadius: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  hangupHide: {
    display: 'none',
  },
});
